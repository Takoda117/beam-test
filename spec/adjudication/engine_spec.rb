require "spec_helper"

RSpec.describe Adjudication::Engine do
  it "has a version number" do
    expect(Adjudication::Engine::VERSION).not_to be nil
  end

  it "returns " do
    #expect(false).to eq(true)
  end
end

RSpec.describe Adjudication::Engine::Adjudicator do
  patient = { "ssn"  =>  "000-99-9876", "relationship"  =>  "self" }
  sub = { "ssn"  =>  "000-99-9876", "group_number"  =>  "US00123" }
  line_item = { "procedure_code"  =>  "D2140", "tooth_code"  =>  6, "charged"  =>  169 }
  claim = { "npi"  =>  "1073702056", "number"  =>  "2017-09-10-112494", "start_date"  =>  "2017-09-10", "subscriber"  =>  sub, "patient" =>  patient, "line_items"  =>  [ line_item ] }
  
  it 'check_against_adjucation_rules? should return false for claims without valid provider or are duplicate and pass for all others' do
    no_prov_claim_obj = Adjudication::Engine::Claim.new(claim)
    claim['provider'] = true
    claim_obj = Adjudication::Engine::Claim.new(claim)
    dup_claim = Adjudication::Engine::Claim.new(claim)

    adjudicator = Adjudication::Engine::Adjudicator.new
    expect(adjudicator.check_against_adjucation_rules?(no_prov_claim_obj)).to be false
    expect(adjudicator.check_against_adjucation_rules?(claim_obj)).to be true
    expect(adjudicator.check_against_adjucation_rules?(dup_claim)).to be false
  end
end
