require "spec_helper"

RSpec.describe Adjudication::Providers do
  it "open returns data from a url" do
    expect(Adjudication::Providers::Fetcher.open('https://www.commonswap.com')).not_to be nil
  end

  it "provider_data to return csv data from beam" do
    num_of_headers = Adjudication::Providers::Fetcher.new.provider_data()[/.+\n/].split(',').length
    expect(num_of_headers).to eq(10)
  end
  it "parse_provider_data should return parsed csv as csv hash with 'First Name' and NPI keys" do
    fake_csv =
    'First Name,NPI
    any,text'
    parsed_csv = Adjudication::Providers::Fetcher.new.parse_provider_data(fake_csv)
    expect(parsed_csv[0]['first_name']).not_to be nil
    expect(parsed_csv[0]['npi']).not_to be nil
  end
  it "clean_provider_data should return hash keyed by nil" do
    fake_parsed_csv = []
    fake_parsed_csv.push({'first_name' => 'any', 'npi' => '1234567890'})
    fake_parsed_csv.push({'first_name' => 'text', 'npi' => '12567890'})
    cleaned_csv_data = Adjudication::Providers::Fetcher.new.clean_provider_data(fake_parsed_csv)
    expect(cleaned_csv_data.size).to be 1
  end
end
