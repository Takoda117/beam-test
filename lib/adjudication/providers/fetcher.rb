require 'net/http'
require 'uri'
require 'csv'

module Adjudication
  module Providers
    class Fetcher
      def self.open(url)
        Net::HTTP.get(URI.parse(url))
      end
      def provider_data()
        Fetcher.open('http://provider-data.beam.dental/beam-network.csv')
      end
      def parse_provider_data data
        parsed_csv = CSV.parse(data)
        headers = parsed_csv.shift.map {|f| f.downcase.gsub(" ", "_")}
        string_data = parsed_csv.map {|row| row.map {|cell| cell.to_s } }
        string_data.map {|row| Hash[*headers.zip(row).flatten] }
      end
      def clean_provider_data data
        npi_hash = Hash.new
        npi_clean_regex = /^[0-9]{10}$/
        for entry in data do 
          if (entry['npi'] && entry['npi'].match(npi_clean_regex))
            npi_hash[entry['npi']] = entry
          else
            STDERR.puts 'NPI invalid for: ' + entry.inspect
          end
        end
        npi_hash
      end
    end
  end
end
