require "adjudication/engine/version"
require "adjudication/providers"
require "adjudication/engine/adjudicator"
require "adjudication/engine/claim"

module Adjudication
  module Engine
    def self.run claims_data
      fetcher = Adjudication::Providers::Fetcher.new
      provider_data = fetcher.provider_data
      parsed_data = fetcher.parse_provider_data(provider_data)
      provider_data_by_npi = fetcher.clean_provider_data(parsed_data)

      adjudicator = Adjudication::Engine::Adjudicator.new
      for claim in claims_data do
        claim['provider'] = provider_data_by_npi[claim['npi']]
        adjudicator.adjudicate(claim)
      end

      adjudicator.processed_claims
    end
  end
end
