module Adjudication
  module Engine
    class Adjudicator
      attr_reader :processed_claims
      :claim_hash

      def initialize
        @processed_claims = []
        @claim_hash = Hash.new
      end

      def check_against_adjucation_rules?(claim)
        if (!claim.provider)
          STDERR.puts 'Claim denied - out of network: ' + claim.number
          return false
        end
        
        unique_claim_idx = claim.patient['ssn'] + '.' + claim.start_date + '.' + claim.procedure_codes.reduce('') {|str, code| str + code}
        if (@claim_hash[unique_claim_idx]) #already exists
          STDERR.puts 'Claim denied - duplicate: ' + claim.number
          return false
        end

        @claim_hash[unique_claim_idx] = true
        return true
      end

      def adjudicate(claim)
        claimObj = Adjudication::Engine::Claim.new(claim)

        isValidClaim = check_against_adjucation_rules?(claimObj)

        for line_item in claimObj.line_items
          if (!isValidClaim) #Could move this out of for loop and make it a return to completely remove invalid claims from processed claims
            line_item.reject!
            next
          end

          if (line_item.preventive_and_diagnostic?)#Fully covered claim
            line_item.pay!(line_item.charged)
            next
          end

          if (line_item.ortho?)
            line_item.pay!(line_item.charged/4)
            next
          end

          line_item.reject!
        end

        @processed_claims.push(claimObj)
      end
    end
  end
end
